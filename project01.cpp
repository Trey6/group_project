// ******************* 1. Documentation *******************
// Project name: Project 01 - CSCI B365 - Computer Graphics
// Project goal: Draw colorful figure(s) in one display windows using 2D graphics primitives
// Author's name: Lingtao Chen; Tresvant Robinson
// Date: March 02, 2020


// ********* 2. Library and Header files ******************
#include <iostream>
//#include <cmath>		// using sin and cos

#ifdef __APPLE__
#  include <GLUT/glut.h>
#else
#  include <GL/glut.h>
#endif

using namespace std;

// Precise PI value
const double PI = atan(1.0)*4;

// *********** 3. Function prototypes **************
void Init();
void myBody(double x1, double y1, double x2, double y2);
void myLeg(double x1, double y1, double x2, double y2);
void myShoe(double x1, double y1, double x2, double y2);
void drawEllipse(double centerX, double centerY, double radiusX, double radiusY, double segmentsNum, double shapeControl, double initialTheta);
void myFace(double centerX, double centerY, double radiusX, double radiusY, double segmentsNum, double shapeControl, double initialTheta);
void myEye(double centerX, double centerY, double radiusX, double radiusY, double segmentsNum, double shapeControl, double initialTheta);
void myEyeBrow(double lineWidth, double x1, double y1, double x2, double y2);
void myHair(double x1, double y1, double x2, double y2, double x3, double y3);
void myTieTop(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4);
void myTieBottom(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double x5, double y5);
void myArm(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4);
void myHand(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4);

//some callback functions that may be useful
void display();
//void reshape(int newWidth, int newHeight);
//void mouse (int, int, int, int);
//void motion (int, int);
//void myKeyboard(unsigned char key, int x, int y);
//void mySpecialKeys(int key, int x, int y);
//void animate(int value);

//global variables for window size
int windowPositionX = 140, windowPositionY = 100;
int windowWidth = 700, windowHight = 700;


// ******* 4. The main function *******************
// 
int main (int argc, char** argv)
{
	glutInit (&argc, argv);                         // Initialize GLUT.
	glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);   // Set display mode.
	glutInitWindowPosition (windowPositionX, windowPositionY);				// Set top-left display-window position.
	glutInitWindowSize (windowWidth, windowHight);					// Set display-window width and h;eight.
	glutCreateWindow ("Project 01 - CSCI B365 - Computer Graphics");	// Create display window.

	Init ( );                            // Execute initialization procedure.

	glutDisplayFunc (display);			// Send graphics to display window.
	//glutReshapeFunc (reshape);
	//glutMouseFunc(mouse);
	//glutMotionFunc(motion);
	//glutKeyboardFunc(myKeyboard);
	//glutSpecialFunc(mySpecialKeys);
	//glutTimerFunc(5,animate,1);


	glutMainLoop ( );                    // Display everything and wait.

	// Never reach here.
	return 0;
}


// ******* 5. Function implementations *************

// Action: Perform the one-time jobs
// Input: None
// Output: None
void Init (void)
{
	double colorRed = 1.0, colorGreen = 1.0, colorBlue = 1.0, alphaValue = 0.0;
	double clippingPlanLeft = 0.0, clippingPlanRight = 700.0;
	double clippingPlanBottom = 0.0, clippingPlanTop = 700.0;
	// ...
	glClearColor (colorRed, colorGreen, colorBlue, alphaValue);   //  Display-window color = white.

	glMatrixMode(GL_PROJECTION);

	//gluOrtho2D sets up a two-dimensional orthographic viewing region, i.e., the clipping plane)
	gluOrtho2D(clippingPlanLeft, clippingPlanRight, clippingPlanBottom, clippingPlanTop); 
}

// Action: 
//			1. Clear the display buffer
//			2. ... (Something specific to this program)
//			3. Flush the buffer to display the image in the display window
// Input: None
// Output: None
void display(void) {

	glClear (GL_COLOR_BUFFER_BIT);

	// draw the body
	glColor3d(0.63, 0.32, 0.18);
	myBody(280, 250, 420, 450);

	// draw the legs
	glColor3d(0.28, 0.24, 0.54);
	myLeg(300, 100, 335, 250);
	myLeg(365, 100, 400, 250);

	// draw the shoes
	glColor3d(0.11, 0.12, 0.32);
	myShoe(255, 85, 335, 100);
	myShoe(445, 85, 365, 100);

	// draw the face
	glColor3d(1.0, 0.89, 0.80);
	myFace(350, 508, 60, 60, 1000, -1, 0);
	myFace(350, 511, 60, 100, 1000, 1, 0);

	// draw the eyes
	glColor3d(0, 0, 0);
	myEye(330, 540, 5, 12, 1000, 2, 0);
	myEye(370, 540, 5, 12, 1000, 2, 0);

	// draw the eyebrows
	glColor3d(0, 0, 0);
	myEyeBrow(4, 320, 570, 349.5, 555);
	myEyeBrow(4, 380, 570, 350.5, 555);

	// draw the hair
	glColor3d(0, 0, 0);
	myHair(333, 605, 327, 615, 340, 607);
	myHair(340, 607, 339, 627, 346, 609);
	myHair(346, 609, 351, 631, 356, 609);
	myHair(356, 609, 366, 624, 364, 607);

	// draw the tie
	glColor3d(1.0, 0, 0);
	myTieTop(330, 452, 342, 438, 355, 439, 365, 450);
	myTieBottom(342, 438, 330, 335, 350, 315, 365, 335, 355, 439);

	// draw the arms
	glColor3d(0.63, 0.32, 0.18);
	myArm(250, 440, 220, 280, 250, 260, 280, 420); // left arm
	myArm(450, 440, 420, 420, 450, 260, 480, 280); // right arm

	// draw the hands
	glColor3d(1.0, 0.89, 0.80);
	myHand(213, 240, 220, 280, 250, 260, 243, 220); // left hand
	myHand(457, 215, 450, 260, 480, 280, 487, 240); // right hand

	glFlush ( );
}

// Action: 
// Input: None
// Output: None
void myBody(double x1, double y1, double x2, double y2){

	cout << "myBody is called." << endl;

	glRectd(x1, y1, x2, y2);

}

// Action: 
// Input: None
// Output: None
void myLeg(double x1, double y1, double x2, double y2){

	cout << "myleg is called." << endl;

	glRectd(x1, y1, x2, y2);

}

// Action: 
// Input: None
// Output: None
void myShoe(double x1, double y1, double x2, double y2){

	cout << "myShoe is called." << endl;

	glRectd(x1, y1, x2, y2);

}

// Action: 
// Input: None
// Output: None
void drawEllipse(double centerX, double centerY, double radiusX, double radiusY, double segmentsNum, double shapeControl, double initialTheta){

	cout << "drawEllipse is called" << endl;

	double theta = initialTheta;
	double thetaIncrement = shapeControl * PI / segmentsNum;
	
	glBegin(GL_POLYGON);
	for(int i = 0; i < segmentsNum; i++){
		glVertex2d(centerX + radiusX * cos(theta), centerY + radiusY * sin(theta));
		theta += thetaIncrement;
		glVertex2d(centerX + radiusX * cos(theta), centerY + radiusY * sin(theta));
	}
	glEnd();

}

// Action: 
// Input: None
// Output: None
void myFace(double centerX, double centerY, double radiusX, double radiusY, double segmentsNum, double shapeControl, double initialTheta){

	cout << "myFace is called" << endl;

	drawEllipse(centerX, centerY, radiusX, radiusY, segmentsNum, shapeControl, initialTheta);

}

// Action: 
// Input: None
// Output: None
void myEye(double centerX, double centerY, double radiusX, double radiusY, double segmentsNum, double shapeControl, double initialTheta){

	cout << "myEye is called" << endl;

	drawEllipse(centerX, centerY, radiusX, radiusY, segmentsNum, shapeControl, initialTheta);

}

// Action: 
// Input: None
// Output: None
void myEyeBrow(double lineWidth, double x1, double y1, double x2, double y2){

	cout << "myEyeBrow is called" << endl;

	glLineWidth(lineWidth);

	glBegin(GL_LINES);
		glVertex2d(x1, y1);
		glVertex2d(x2, y2);
	glEnd();

}

// Action: 
// Input: None
// Output: None
void myHair(double x1, double y1, double x2, double y2, double x3, double y3){

	cout << "myHair is called" << endl;

	glBegin(GL_TRIANGLES);
		glVertex2d(x1, y1);
		glVertex2d(x2, y2);
		glVertex2d(x3, y3);
	glEnd();

}

// Action: 
// Input: None
// Output: None
void myTieTop(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4){

	cout << "myTieTop is called" << endl;

	glBegin(GL_POLYGON);
		glVertex2d(x1, y1);
		glVertex2d(x2, y2);
		glVertex2d(x3, y3);
		glVertex2d(x4, y4);
	glEnd();

}

// Action: 
// Input: None
// Output: None
void myTieBottom(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double x5, double y5){

	cout << "myTieBottom is called" << endl;

	glBegin(GL_POLYGON);
		glVertex2d(x1, y1);
		glVertex2d(x2, y2);
		glVertex2d(x3, y3);
		glVertex2d(x4, y4);
		glVertex2d(x5, y5);
	glEnd();

}

// Action: 
// Input: None
// Output: None
void myArm(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4){

	cout << "myArm is called" << endl;

	glBegin(GL_POLYGON);
		glVertex2d(x1, y1);
		glVertex2d(x2, y2);
		glVertex2d(x3, y3);
		glVertex2d(x4, y4);
	glEnd();

}

// Action: 
// Input: None
// Output: None
void myHand(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4){

	cout << "myHand is called" << endl;

	glBegin(GL_POLYGON);
		glVertex2d(x1, y1);
		glVertex2d(x2, y2);
		glVertex2d(x3, y3);
		glVertex2d(x4, y4);
	glEnd();

}



// Below are implementations of other callback functions
// ...